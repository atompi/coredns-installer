# CoreDNS Installer

Install CoreDNS with binary.

## install

```
chmod 755 bin/coredns
mkdir -p /opt/coredns
cp -r * /opt/coredns/
cp coredns.service /lib/systemd/system/coredns.service
systemctl daemon-reload
systemctl enable --now coredns
```
