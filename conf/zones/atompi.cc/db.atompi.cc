$TTL 3600
$ORIGIN atompi.cc.

@ IN SOA ns1.atompi.cc. atompi.atompi.cc. (
    2021102821 ; Serial
    1H         ; Refresh
    600        ; Retry
    7D         ; Expire
    600        ; Negative Cache TTL
)

@ IN NS ns1

ns1 IN A 192.168.15.137

; Custome
@                 IN A     192.168.15.137
*                 IN CNAME @
kube-apiserver    IN A     192.168.15.130
kube-dashboard    IN A     192.168.15.130
hub               IN A     192.168.15.137
